@ECHO OFF
IF "%1"=="execute" GOTO :EXECUTE ELSE GOTO :STARTLOGGER

REM ---------------------------------------------------------------------------------
:STARTLOGGER
REM ---------------------------------------------------------------------------------

REM "mydate": get date stamp
For /f "tokens=1-3 delims=- " %%a in ('date /t') do (set mydate=%%a-%%b-%%c)

call "%0" execute 1>> CrawlingModulePermissionExpansionTool_%mydate%.log 2>>&1

GOTO :END

REM ---------------------------------------------------------------------------------
:EXECUTE
REM ---------------------------------------------------------------------------------

SET PERMISSIONEXPANDER=Coveo.Connectors.Tools.OnPremisesPermissionExpander.exe
pushd ..\bin

"%PERMISSIONEXPANDER%" --verbose -c ..\config.json --entityStates UpToDate,NotUpdated,OutOfDate,InError,Disabled

popd

:END