REM @ECHO OFF
REM ***************************************************************************
REM *** YOUR CONFIGURATION: SET YOUR SERVICE NAME AND OPTIONNALLY A PORT
REM *** PORT: PORT EMPTY IF YOU ONLY HAVE ONE SERVICE ON THE SYSTEM
REM *** PORT: IF MORE THAN ONE SERVICE, USE PORTS 52841, 52842, 52843, etc...
SET SERVICENAME=CoveoConnector-KM-NAZ-Getdoc
SET SERVICEPORT=52841
REM *** END OFYOUR  CONFIGURE 
REM ***************************************************************************

REM ***************************************************************************
REM ***BELOW ARE AUTOMATIONS; DONT MODIFY
pushd %~dp0..
SET SOURCEFOLDER=%CD%\
pushd %~dp0..\..
SET WORKINGFOLDER=%CD%\
popd
popd

SET BINFOLDER=%WORKINGFOLDER%bin\
SET CRAWLERPY=%BINFOLDER%crawler.py
SET CDFNODEPROCESS8=%WORKINGFOLDER%bin\CDFNodeProcess8.exe
SET CRAWLERJSON=%SOURCEFOLDER%crawler.json
SET LOGGERJSON=%SOURCEFOLDER%logger.json

IF "%SERVICEPORT%"=="" (
    SET PORTCLAUSE=
) ELSE (
    SET PORTCLAUSE=-port %SERVICEPORT%
)

REM *** END OF AUTOMATIONS
REM ***************************************************************************
